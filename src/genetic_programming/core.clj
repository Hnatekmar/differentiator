(ns genetic-programming.core
  (:gen-class))

(require '[different.core :as df])
(require '[clojure.math.numeric-tower :as math])

(def expt math/expt)

(def model '(+ (* theta1 x) (* x theta0)))

(def mse (list 'expt (list '- 'y model) 2))

(defn mse-fn [x y theta0 theta1 expt]
  (expt (- y (+ (* theta1 x) theta0)) 2))

(defn f [x y theta0 theta1]
  (+ (* theta1 x) theta0))

(defn gradient-step
  [model weights alpha guess x y]
  (let [derivation (map #(df/diff % model) weights)]
    (for [el derivation]
      (do 
        (def args (apply vector (concat weights '[x y expt])))
        (def wargs (concat guess [x y expt]))
        (let [delta (eval (list 'fn args el))]
          (* alpha (apply delta wargs)))))))

(defn optimize
  [model weights alpha guess dataset iters]
  (let [delta (for [[x y] dataset]
                (gradient-step
                  model
                  weights
                  alpha
                  guess
                  x
                  y))
        deltaSum (reduce #(map + %1 %2) delta)]
    (if (<= iters 0)
      guess
      (recur
        model
        weights 
        alpha
        (doall (map + guess deltaSum))
        dataset
        (dec iters)))))


(defn apply-and-return [x f]
  (map (fn [el]
         (list el (f el))) x))


(defn -main
  [& args]
  (doall
    (loop [guess [0 0.5]]
      (println guess)
      (recur (optimize mse '[theta0 theta1] 0.0001 guess
                       [[1 2]
                        [-1 -2]]
                           500)))))
